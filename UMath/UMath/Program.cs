﻿using System;
using System.Linq;
using UMath.Core.Services;

namespace UMath
{
    public class Program
    {
        static void Main(string[] args)
        {
            var service = new MathOperationService();

            Console.WriteLine("Boas vindas ao UMath!");
            Console.WriteLine("Digite o valor que deseja realizar o calculo:");

            int number = 0;
            
            while(!int.TryParse(Console.ReadLine(), out number) || number <= 0)
            {
                Console.WriteLine("Digite apenas números inteiros positivos");
            }

            var result = service.DecomposeNumber(number);
            if(result != null)
            {
                Console.WriteLine("Os números divisores são {0}", String.Join(", ", result.Dividers));
                Console.WriteLine(string.Concat(Enumerable.Repeat("-", 40)));
                Console.WriteLine("Os números primos são {0}", String.Join(", ", result.CousinDividers));
                Console.WriteLine(string.Concat(Enumerable.Repeat("-", 40)));
            }
            else
            {
                Console.WriteLine("Não foi possível calcular");
            }
        }
    }
}
