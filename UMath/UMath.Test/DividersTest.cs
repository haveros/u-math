using UMath.Core.Models;
using UMath.Core.Services;
using Xunit;

namespace UMath.Test
{
    public class DividersTest
    {
        [Fact]
        public void SuccessDividersDecompose()
        {
            var service = new MathOperationService();
            var ret = service.DecomposeNumber(45);
            var expected = new DecomposeReturnModel()
            {
                Dividers = new() { 1, 3, 5, 9, 15, 45 }
            };

            Assert.Equal(expected.Dividers, ret.Dividers);
        }

        [Fact]
        public void SuccessDecomposeCousinDivisors()
        {
            var service = new MathOperationService();
            var ret = service.DecomposeNumber(45);
            var expected = new DecomposeReturnModel()
            {
                CousinDividers = new() { 1, 3, 5 }
            };

            Assert.Equal(expected.CousinDividers, ret.CousinDividers);
        }

        [Fact]
        public void MaxDecompose()
        {
            var service = new MathOperationService();
            var ret = service.DecomposeNumber(int.MaxValue);

            Assert.NotNull(ret);
        }

        [Fact]
        public void DecomposeNegativeNumber()
        {
            var service = new MathOperationService();
            var ret = service.DecomposeNumber(-45);

            Assert.Null(ret);
        }
    }
}
