﻿using UMath.Core.Models;

namespace UMath.Core.Interfaces
{
    public interface IMathOperationService
    {
        DecomposeReturnModel DecomposeNumber(int number);
    }
}
