﻿using UMath.Core.Interfaces;
using UMath.Core.Models;

namespace UMath.Core.Services
{
    public class MathOperationService : IMathOperationService
    {
        /// <summary>
        /// Retorna todos os divisores e números primos do parametro <paramref name="number"/>
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public DecomposeReturnModel DecomposeNumber(int number)
        {
            if (number <= 0)
                return null;

            var ret = new DecomposeReturnModel();

            for (int divisor = 2; divisor <= (number / 2); divisor++)
            {
                if ((number % divisor) == 0)
                {
                    if (IsCousin(divisor))
                        ret.CousinDividers.Add(divisor);

                    ret.Dividers.Add(divisor);
                }
            }

            ret.Dividers.Add(number);

            return ret;
        }

        private static bool IsCousin(int n)
        {
            if (n == 2) return true;

            if (n < 2 || (n % 2 == 0)) return false;

            for (int x = 3; x * x <= n; x += 2)
                if (n % x == 0) return false;

            return true;
        }
    }
}
