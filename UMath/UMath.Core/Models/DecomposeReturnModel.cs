﻿using System.Collections.Generic;

namespace UMath.Core.Models
{
    public class DecomposeReturnModel
    {
        public List<int> Dividers { get; set; }
        public List<int> CousinDividers { get; set; }

        public DecomposeReturnModel()
        {
            Dividers = new() { { 1 } };
            CousinDividers = new() { { 1 } };
        }
    }
}
