﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UMath.Api.Models
{
    public class DividersModel
    {
        [Required(ErrorMessage = "Número é obrigatório")]
        [Range(1, Int32.MaxValue, ErrorMessage = "O valor deve ser maior ou igual a 1")]
        public int Number { get; set; }
    }
}
