﻿using Microsoft.AspNetCore.Mvc;
using UMath.Api.Models;
using UMath.Core.Interfaces;
using UMath.Core.Models;

namespace UMath.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DividersController : ControllerBase
    {
        private readonly IMathOperationService _mathService;
        public DividersController(IMathOperationService mathService)
        {
            _mathService = mathService;
        }

        /// <summary>
        /// serviço de calculo de divisores e divisores primos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public DecomposeReturnModel Get([FromQuery] DividersModel model)
        {
            return _mathService.DecomposeNumber(model.Number);
        }
    }
}
