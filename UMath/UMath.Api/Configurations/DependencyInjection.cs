﻿using Microsoft.Extensions.DependencyInjection;
using UMath.Core.Interfaces;
using UMath.Core.Services;

namespace UMath.Api.Configurations
{
    public static class DependencyInjection
    {
        public static void AddDependencies(this IServiceCollection services)
        {
            services.AddScoped<IMathOperationService, MathOperationService>();
        }
    }
}
