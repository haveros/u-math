# Titulo do Projeto

UMath

## Descrição do projeto

O projeto tem o objetivo de oferecer serviços de operações matemáticas, para facilitar o processamento de calculos númericos.
Na versão 1.0, focamos na operação de decomposição de números, trazendo resultados de divisores e divisores primos.

### Pré Requisitos


* [.Net Core 5.0](https://dotnet.microsoft.com/en-us/download/dotnet/5.0)
* [Visual Studio 2019 ou superior](https://visualstudio.microsoft.com/pt-br/vs/)

## Como utilizar

● Dado um número de entrada, o programa irá calcular todos os divisores que compõem o número e também seus divisores primos.

O projeto pode ser executado em sua versão de console ou versão web.

## Exemplo de Console:

Digite o valor que deseja realizar o calculo: 
#### 45
- Os números divisores são 1, 3, 5, 9, 15, 45
- Os números primos são 1, 3, 5
----------------------------------------

### Exemplo versão web (CURL)
<pre>
 curl "https://localhost:44327/Dividers?Number=45" ^
  -H "authority: localhost:44327" ^
  -H "sec-ch-ua: ^\^" Not A;Brand^\^";v=^\^"99^\^", ^\^"Chromium^\^";v=^\^"98^\^", ^\^"Google Chrome^\^";v=^\^"98^\^"" ^
  -H "accept: text/plain" ^
  -H "sec-ch-ua-mobile: ?0" ^
  -H "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.81 Safari/537.36" ^
  -H "sec-ch-ua-platform: ^\^"Windows^\^"" ^
  -H "sec-fetch-site: same-origin" ^
  -H "sec-fetch-mode: cors" ^
  -H "sec-fetch-dest: empty" ^
  -H "referer: https://localhost:44327/swagger/index.html" ^
  -H "accept-language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7" ^
  -H "cookie: ai_user=0c9Qy^|2022-02-02T20:58:53.154Z; intercom-session-ug1cxwkd=OVRpVU9BNVNMSFNBTlB6M1hMWm1tNXUyUUxodmpkdEhSSUFNTW9uNVdCbmdFeE83YnNZc0lqYUtJR215M0dtZS0tN3JrcFVVdHU3dkJISFREeUZsdC9HUT09--31d96a7b76b9e441487eb72fedd8f113a53d449a" ^
  --compressed
</pre>

### Retorno JSON

<pre>
{
  "dividers": [
    1,
    3,
    5,
    9,
    15,
    45
  ],
  "cousinDividers": [
    1,
    3,
    5
  ]
}
</pre>
## Versão

Versão 1.0

## Autor

* **Mateus Primo Cardoso**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
